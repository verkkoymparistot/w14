import React, { useState } from 'react';
import './App.css';

const App = () => {
  const [newChat, setChat] = useState('');
  const [showMessages, setShowMessages] = useState(false);
  
  const send = () => {
    if (newChat !== '') {
      const existingChats = JSON.parse(localStorage.getItem('chats')) || [];
      
      existingChats.push(newChat);
  
      localStorage.setItem('chats', JSON.stringify(existingChats));
    }
  };

  const show = () => {
    setShowMessages(true);    
  };
  
  return (
    <div>
      <div className="chat">
        <input
          type="text"
          id="chat"
          value={newChat}
          onChange={(e) => setChat(e.target.value)}
        />
        <button onClick={send}>Send</button>
        <button onClick={show}>Show messages</button>
      </div>

      {showMessages && (
          <div className='paragraphs'>
          <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eget egestas purus viverra accumsan in. A arcu cursus vitae congue mauris. Ultricies mi quis hendrerit dolor magna eget est lorem. Diam vulputate ut pharetra sit amet aliquam id diam maecenas. Tellus cras adipiscing enim eu turpis egestas pretium. Lectus mauris ultrices eros in cursus. Vulputate eu scelerisque felis imperdiet proin fermentum leo. Lectus vestibulum mattis ullamcorper velit sed ullamcorper morbi. Cras fermentum odio eu feugiat pretium. Quis imperdiet massa tincidunt nunc pulvinar sapien. Sit amet consectetur adipiscing elit ut aliquam.
          </p>
          <p>
          Dictum sit amet justo donec enim diam. Aliquam ultrices sagittis orci a scelerisque. Felis imperdiet proin fermentum leo vel orci porta. Neque vitae tempus quam pellentesque. Egestas erat imperdiet sed euismod nisi porta lorem. Vivamus at augue eget arcu dictum. Ullamcorper velit sed ullamcorper morbi tincidunt. Lobortis feugiat vivamus at augue eget arcu dictum varius. Etiam dignissim diam quis enim lobortis scelerisque. Porta nibh venenatis cras sed felis eget velit aliquet sagittis.
          </p>
          <p>
          Dictum sit amet justo donec enim diam. Aliquam ultrices sagittis orci a scelerisque. Felis imperdiet proin fermentum leo vel orci porta. Neque vitae tempus quam pellentesque. Egestas erat imperdiet sed euismod nisi porta lorem. Vivamus at augue eget arcu dictum. Ullamcorper velit sed ullamcorper morbi tincidunt. Lobortis feugiat vivamus at augue eget arcu dictum varius. Etiam dignissim diam quis enim lobortis scelerisque. Porta nibh venenatis cras sed felis eget velit aliquet sagittis.
          </p>
          <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eget egestas purus viverra accumsan in. A arcu cursus vitae congue mauris. Ultricies mi quis hendrerit dolor magna eget est lorem. Diam vulputate ut pharetra sit amet aliquam id diam maecenas. Tellus cras adipiscing enim eu turpis egestas pretium. Lectus mauris ultrices eros in cursus. Vulputate eu scelerisque felis imperdiet proin fermentum leo. Lectus vestibulum mattis ullamcorper velit sed ullamcorper morbi. Cras fermentum odio eu feugiat pretium. Quis imperdiet massa tincidunt nunc pulvinar sapien. Sit amet consectetur adipiscing elit ut aliquam.
          </p>
          <p>
          Sed id semper risus in. Adipiscing elit ut aliquam purus sit amet luctus. Viverra aliquet eget sit amet. Ut aliquam purus sit amet luctus venenatis lectus magna. Mi bibendum neque egestas congue. Lacus sed turpis tincidunt id aliquet. Nulla malesuada pellentesque elit eget gravida. Volutpat blandit aliquam etiam erat velit. Feugiat vivamus at augue eget arcu. Vitae sapien pellentesque habitant morbi tristique senectus et netus. Nullam ac tortor vitae purus faucibus ornare suspendisse sed nisi. Tincidunt praesent semper feugiat nibh sed pulvinar proin gravida. Eleifend mi in nulla posuere. Faucibus a pellentesque sit amet porttitor eget dolor morbi non.
          </p>
        </div>
      )};
    </div>
  );
};

export default App;